################################################################################
#   Update Bump Distance
################################################################################

def updateBumpDistance(self, context):
    bumpnode = context.object.active_material.node_tree.nodes.get("BumpNormal")
    if bumpnode is not None:
        print("Bump Distance: "+ context.scene.prop_BumpDistance)
        bumpnode.inputs[1].default_value = context.scene.prop_BumpDistance