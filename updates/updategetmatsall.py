################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy


################################################################################
#   Update GetMatsAll
################################################################################

def updateGetMatsAll(self, context):
    ob = context.active_object
    mats = bpy.data.materials
    selected_mat = context.scene.prop_MatsAllDropDown
    if selected_mat != 'None':
        print("dropdown (allmats) mat: "+selected_mat)
        ob.data.materials.append(mats[selected_mat])