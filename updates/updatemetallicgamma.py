################################################################################
#   Update Metallic Gamma
################################################################################

def updateMetallicGamma(self, context):
    gammanode = context.object.active_material.node_tree.nodes.get("GammaMetallic")
    if gammanode is not None:
        print("Metallic Gamma: "+ context.scene.prop_MetallicGamma)
        gammanode.inputs[1].default_value = context.scene.prop_MetallicGamma