################################################################################
#   Update Bump Invert
################################################################################

def updateBumpInvert(self, context):
    bumpnode = context.object.active_material.node_tree.nodes.get("BumpNormal")
    if bumpnode is not None:
        print("Bump Invert: "+ context.scene.prop_BumpInvert)
        bumpnode.invert = context.scene.prop_BumpInvert