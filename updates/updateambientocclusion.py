################################################################################
#   Update Ambient Occlusion
################################################################################

def updateAmbientOcclusion(self, context):
    mixrgbnode = context.object.active_material.node_tree.nodes.get("MixRGBAO")
    if mixrgbnode is not None:
        print("Ambient Occlusion: "+ context.scene.prop_AOFac)
        mixrgbnode.inputs[0].default_value = context.scene.prop_AOFac