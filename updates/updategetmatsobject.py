################################################################################
#   Update GetMatsObject
################################################################################

def updateGetMatsObject(self, context):
    ob = context.active_object
    selected_mat = context.scene.prop_MatsObjDropDown
    mat_dict = {mat.name: i for i, mat in enumerate(context.object.data.materials)}
    if selected_mat != 'None':
        if ob.data.materials:
            if(ob.data.materials.get(selected_mat) is not None):
                print("dropdown (objmats) mat: "+selected_mat)
                ob.active_material_index = mat_dict[selected_mat]