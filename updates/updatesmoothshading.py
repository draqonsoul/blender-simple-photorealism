################################################################################
#   Update Smooth Shading
################################################################################

def updateUseSmoothShading(self, context):
    Object = context.object
    Mesh = Object.data
    UseSmoothShading = context.scene.prop_UseSmoothShading
    print("smooth shading: "+ str(UseSmoothShading))
    for Face in Mesh.polygons:
        Face.use_smooth = UseSmoothShading