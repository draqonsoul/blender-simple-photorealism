################################################################################
#   Update Roughness Gamma
################################################################################

def updateRoughnessGamma(self, context):
    gammanode = context.object.active_material.node_tree.nodes.get("GammaRoughness")
    if gammanode is not None:
        print("Roughness Gamma: "+ context.scene.prop_OpacityGamma)
        gammanode.inputs[1].default_value = context.scene.prop_RoughnessGamma