################################################################################
#   Update Bump
################################################################################

def updateBumpStrength(self, context):
    bumpnode = context.object.active_material.node_tree.nodes.get("BumpNormal")
    if bumpnode is not None:
        print("Bump Strength: "+ context.scene.prop_BumpStrength)
        bumpnode.inputs[0].default_value = context.scene.prop_BumpStrength