################################################################################
#   Update Displace Strength
################################################################################

def updateDisplaceStrength(self, context):
    Modifier = context.object.modifiers.get("Displace", None)
    if Modifier is not None:
        print("Displace Strength: "+ context.scene.prop_DisplaceStrength)
        Modifier.strength = context.scene.prop_DisplaceStrength