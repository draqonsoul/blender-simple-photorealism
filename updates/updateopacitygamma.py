################################################################################
#   Update Opacity Gamma
################################################################################

def updateOpacityGamma(self, context):
    gammanode = context.object.active_material.node_tree.nodes.get("GammaOpacity")
    if gammanode is not None:
        print("Opacity Gamma: "+ context.scene.prop_OpacityGamma)
        gammanode.inputs[1].default_value = context.scene.prop_OpacityGamma