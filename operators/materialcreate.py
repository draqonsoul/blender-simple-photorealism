################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy
from bpy.types import Operator


################################################################################
#   Material Create Operator
################################################################################

class OperatorMaterialCreate(Operator):
    bl_idname = "object.createmat_operator"
    bl_label = "CreateMat Operator"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def execute(self, context):
        matname = context.scene.prop_MatName
        mat = bpy.data.materials.get(matname)
        if mat is None:
            mat = bpy.data.materials.new(name=matname)
        mat.use_nodes = True
        if context.object.data.materials:
            if(context.object.data.materials.get(matname, None) is None):
                context.object.active_material = mat
            else: 
                context.object.material_slots[matname].material = mat
        else:
            context.object.data.materials.append(mat)

        return {'FINISHED'}