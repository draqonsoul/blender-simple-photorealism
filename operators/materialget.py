################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
import bpy
from bpy.types import Operator


################################################################################
#   Material Get Operator
################################################################################

class OperatorMaterialGet(Operator):
    bl_idname = "object.getmaterial_operator"
    bl_label = "Test Operator"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        context.scene.prop_UseSmoothShading = context.object.data.polygons[0].use_smooth
        SubdivisionModifier = context.object.modifiers.get("Subdivide", None)
        DisplaceModifier = context.object.modifiers.get("Displace", None)
        
        if(SubdivisionModifier is not None):
            context.scene.prop_SubdivisionLevel = SubdivisionModifier.levels
        else:
            context.scene.prop_SubdivisionLevel = 0

        if(DisplaceModifier is not None):
            context.scene.prop_DisplaceMidlevel = DisplaceModifier.mid_level
            context.scene.prop_DisplaceStrength = DisplaceModifier.strength

        if context.object.active_material is not None:
            if context.object.active_material.node_tree is not None:
                bumpnode = context.object.active_material.node_tree.nodes.get("BumpNormal")
                GammaRoughness = context.object.active_material.node_tree.nodes.get("GammaRoughness")
                GammaMetallic = context.object.active_material.node_tree.nodes.get("GammaMetallic")
                GammaOpacity = context.object.active_material.node_tree.nodes.get("GammaOpacity")
                mappingnode = context.object.active_material.node_tree.nodes.get("Mapping")

                # context.scene.prop_MatName = context.object.active_material.name
        
                if(mappingnode is not None):
                    context.scene.prop_TexScaleX = mappingnode.inputs[3].default_value.x
                    context.scene.prop_TexScaleY = mappingnode.inputs[3].default_value.y
                else: 
                    if(DisplaceModifier is not None):
                        context.scene.prop_TexScaleX = DisplaceModifier.texture.repeat_x
                        context.scene.prop_TexScaleY = DisplaceModifier.texture.repeat_y

                if(bumpnode is not None):
                    context.scene.prop_Bump = bumpnode.inputs[0].default_value
                    context.scene.prop_BumpDistance = bumpnode.inputs[1].default_value
                    context.scene.prop_BumpInvert = bumpnode.invert

                if(GammaRoughness is not None):
                    context.scene.prop_RoughnessGamma = GammaRoughness.inputs[1].default_value
                
                if(GammaMetallic is not None):
                    context.scene.prop_MetallicGamma = GammaMetallic.inputs[1].default_value

                if(GammaOpacity is not None):
                    context.scene.prop_OpacityGamma = GammaOpacity.inputs[1].default_value

        return {'FINISHED'}