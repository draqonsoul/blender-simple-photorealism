################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel


################################################################################
#   Material Settings Sub-Panel
################################################################################

class SubPanelMaterialSettings_General(Panel):
    bl_idname = "OBJECT_PT_PANEL_SUBMATERIAL_GENERAL_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_MAINMATERIAL_SIMPLE_PBR"
    bl_label = "General"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        panel = self.layout
        panel.prop(context.scene, "prop_UseSmoothShading")