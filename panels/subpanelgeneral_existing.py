################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel

# Addon Operators
from ..operators.materialrename import OperatorMaterialRename
from ..operators.materialremove import OperatorMaterialRemove


################################################################################
#   Sub Panel General1
################################################################################

class SubPanelGeneral_Existing(Panel):
    bl_idname = "OBJECT_PT_PANEL_SUBGENERAL_EXISTING_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_MAINGENERAL_SIMPLE_PBR"
    bl_label = "Existing Material"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        panel = self.layout
        if context.object.active_material is None:
            panel.label(text="This object must have a material first.")
        else:
            row = panel.row()
            row.label(text="Switch to existing Material")
            row.prop(context.scene, "prop_MatsObjDropDown")
            row = panel.row()
            row.prop(context.scene, "prop_MatName")
            row.operator(OperatorMaterialRename.bl_idname, text="Rename Material", icon="IMAGE")
            row = panel.row()
            row.operator(OperatorMaterialRemove.bl_idname, text="Unlink Material", icon="IMAGE").unlinkOnly = True
            row.operator(OperatorMaterialRemove.bl_idname, text="Remove Material", icon="IMAGE").unlinkOnly = False