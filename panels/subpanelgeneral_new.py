################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel

# Addon Operators
from ..operators.materialcreate import OperatorMaterialCreate


################################################################################
#   Sub Panel General2
################################################################################

class SubPanelGeneral_New(Panel):
    bl_idname = "OBJECT_PT_PANEL_SUBGENERAL_NEW_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_MAINGENERAL_SIMPLE_PBR"
    bl_label = "New Material"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        panel = self.layout
        row = panel.row()
        row.label(text="Assign Existing Material:")
        row.prop(context.scene, "prop_MatsAllDropDown")
        row = panel.row()
        row.prop(context.scene, "prop_MatName")
        row.operator(OperatorMaterialCreate.bl_idname, text="Create Material", icon="IMAGE")