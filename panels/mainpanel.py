################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel


################################################################################
#   Main Panel
################################################################################

class MainPanel(Panel):
    bl_idname = "OBJECT_PT_PANEL_SIMPLE_PBR"
    bl_label = "Simple Photorealism"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        return
