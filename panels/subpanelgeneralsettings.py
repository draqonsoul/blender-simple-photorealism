################################################################################
#   Imported Libraries
################################################################################

# Internal Libraries
from bpy.types import Panel


################################################################################
#   General Settings Sub-Panel
################################################################################

class SubPanelGeneralSettings(Panel):
    bl_idname = "OBJECT_PT_PANEL_MAINGENERAL_SIMPLE_PBR"
    bl_parent_id = "OBJECT_PT_PANEL_SIMPLE_PBR"
    bl_label = "Manage Materials"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Addons"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.object is not None

    def draw(self, context):
        panel = self.layout