################################################################################
#   Global Variable TimerMatChange
################################################################################

class TimerMatChange(object):
    def __init__(self):
        self._x = None

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @x.deleter
    def x(self):
        del self._x