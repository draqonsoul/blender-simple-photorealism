def getMatsObject(self, context):
        sets = []
        mats = context.object.data.materials
        for mat in mats:
            if mat is not None:
                if mat.is_grease_pencil is False:
                    sets.append((mat.name, mat.name, mat.name))
        if not sets:
            sets = [('None', 'None', 'None')]
        return sets